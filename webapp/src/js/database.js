require(['lib/pouchdb.min'], function(PouchDB){
    var obj = {};
    var db = new PouchDB('contacts');
    
    obj.contactCreate = function(contact){
        if(!contact){
            throw "Error: contact empty";
        }

        return db.put({
            _id:  new Date().toISOString(),
            name: contact.name,
            date: contact.date,
            month: contact.mnth,
            year: contact.year,
            desc: '',
            star: 0
        }).catch(function(e){
            alert(e);
        });
    };    

    obj.contactUpdate = function(contact){
        return db.put(contact).catch(function(e){
            if(e.status===409){
                return obj.contactUpdate(contact);
            }else{
                alert(e); 
            }
        });
    };
    
    obj.contactGet = function(id){
        return db.get(id).catch(function(e){
            console.log(e);
        });
    };
    
    obj.contactDelete = function(contact){
        contact['_deleted'] = true;
        return db.put(contact).catch(function(e){
            if(e.status==409){
                return obj.contactDelete(contact);
            }else{
                alert(e);
            }
        });
    };
    
    obj.contactStar = function(id){
        obj.contactGet(id).then(function(contact){
            if(!contact['star']){ contact['star']=0; }
            contact['star'] = 1 - contact['star'];
            return db.put(contact);
        }).then(function(d){
            window.load_users();
        }).catch(function(e){
            if(e.status==409){ return obj.contactStar(contact['_id']); }
            else{ alert(e); }
        });
    };

    obj.getAll = function(){
        return db.allDocs({include_docs:true}).catch(function(e){
            console.log(e);
        });
    };
    
    obj.db = db;
    window.db = obj;
    window.user = null;
});
