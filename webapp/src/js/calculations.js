function figure(dd, mm, yyyy){
    function mod9(x){
        return x%9 || 9;
    }
   
    var res = {};
    
    // date month year
    res['dd'] = dd;
    res['mm'] = mm;
    res['y1'] = yyyy.slice(0,2);
    res['y2'] = yyyy.slice(2);
    
    var a = parseInt(res.dd);
    var b = parseInt(res.mm);
    var c = parseInt(res.y1);
    var d = parseInt(res.y2);

    // Top 4 values
    res['l'] = mod9(a);
    res['m'] = mod9(b);
    res['n'] = mod9(c);
    res['o'] = mod9(d);

    // Second row of pyramid
    res['p'] = mod9(a+b);
    res['q'] = mod9(c+d);

    // lifepath number
    res['r'] = mod9(a+b+c+d);
    
    // inborn challenge
    res['s'] = mod9(2*a+b);
    res['t'] = mod9(a+2*b);
    res['a'] = mod9(3*a+3*b);

    // inborn strength
    res['u'] = mod9(2*c+d);
    res['v'] = mod9(c+2*d);
    res['b'] = mod9(3*(c+d));

    // first row of bottom
    res['w'] = mod9(a+b+2*(c+d));
    res['x'] = mod9(2*(a+b)+c+d);

    // second row of bottom
    res['c'] = mod9(3*(a+b+c+d));

    // last row of bottom
    res['y'] = mod9(5*(a+b)+4*(c+d));
    res['z'] = mod9(4*(a+b)+5*(c+d));

    // hidden numbers
    res['aa'] = mod9(b+c);
    res['ab'] = mod9(2*(b+c)+a+d);
    res['ac'] = mod9(2*(a+b+c+d));
    res['ad'] = mod9(2*(a+d)+b+c);
    res['ba'] = mod9(2*(a+b));
    res['bb'] = mod9(2*(c+d));
    
    return res;
}

function words(name){
    name = name.toLowerCase();
    var res = name.split('').map(function(c){
        var c = c.charCodeAt(0);
        if(c>=97 && c<=122){
           return (c-96)%9 || 9;
        }
        return 0;
    });
 
    return (res.reduce(function(a, b){return a+b;}))%9 || 9; 
}

function rateName(lifepath){
    var order = [1,2,4,3,5];
    lifepath = order.indexOf((lifepath%5||5));
    
    /* best first */
    var rating = [];
    rating.push(order[lifepath]);
    rating.push(order[(lifepath+2)%5]);
    rating.push(order[(lifepath+4)%5]);
    rating.push(order[(lifepath+1)%5]);
    rating.push(order[(lifepath+3)%5]);

    return rating;
}

function mod9(v){
    return v%9 || 9;
}

function lifepath(dd, mm, yyyy){
    dd = parseInt(dd);
    mm = parseInt(mm);
    y1 = parseInt(yyyy.slice(0,2));
    y2 = parseInt(yyyy.slice(2));

    return (dd+mm+y1+y2)%9 || 9;
}

define(function(){
    var calc = {};
    calc.lifepath = lifepath;
    calc.figure = figure;
    calc.words = words;
    calc.rate = rateName;
    
    return calc;
});
