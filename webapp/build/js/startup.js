/* Javascript for frontend */
function clearAll(){
    $('.mdl-layout').css('display', 'None');
}

$('.mdl-navigation__link').click(function(){
    $('.mdl-layout__obfuscator').click();
});

/* Routing services */
require(["lib/simrou.min"], function(){
    window.router = new Simrou();
    var router = window.router;

    var route_add = router.addRoute('/add');
    route_add.post(function(evt, params){
        if(addFormSubmit()){
            router.navigate('/page/main/contacts');
        }
        else{
            alert('Please fill in all the details.');
        }
    });
    
    var route_panels = router.addRoute("/page/main/:id");
    route_panels.get(function(evt, params){
        router.resolve("/page/main", "GET");
        $(".mdl-layout__tab[href|='#panel-"+params.id+"']")[0].click();
    });

    var route_user = router.addRoute('/page/user/:id');
    route_user.get(function(evt, params){
        router.resolve('/page/user', 'GET');
        window.load_user(params.id);
    });

    var route_main = router.addRoute("/page/:page");
    route_main.get(function(evt, params){
        clearAll();
        $("#pg-"+params.page).css("display", "");

        if(params.page=="main"){
            $('.mdl-layout__tab[href|="#panel-contacts"]')[0].click();
            window.clearData();
        }

        if(params.page=='user'){
            $('.mdl-layout__tab[href|="#panel-figure"]')[0].click();
        }
    });

    router.start("/page/main");
});


