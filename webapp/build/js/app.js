require(['js/calculations.js', 'lib/handlebars.js'], function(calc, Handlebars){
    window.debug = function(){
        eval($('#debug')[0].value);
    };
    
    window.load_users = function(){   
        /* load previous data */
        window.db.getAll().then(function(d){
            var users = d.rows.map(function(row){
                return row.doc;
            });
            
            if(!users.length){
                $('#panel-contacts').html($('#template__users--empty').html());
                $('#panel-lifepath').html($('#template__users--empty').html());
                return false;
            } 

            users = users.map(function(user){
                user.lifepath = calc.lifepath(user.date, user.month, user.year);
                return user;
            });
            
            var template = Handlebars.compile($('#template__users').html());
            
            users.sort(function(a,b){
                if(a.name==b.name){ return 0; }
                else if(a.name>b.name) { return 1; }
                else{ return -1; }
            });
            $('#panel-contacts').html(template({users:users}));

            users.sort(function(a,b){
                return a.lifepath - b.lifepath;
            });
            $('#panel-lifepath').html(template({users:users}));

            users.sort(function(a, b){
                var da = a.month+a.date+a.year;
                var db = b.month+b.date+b.year;
                if(da>db){ return 1; }
                else if(da==db){ return 0; }
                else{ return -1; }
            });
            $('#panel-month').html(template({users:users}));
        });
    };
    
    /* Loading user details */
    window.load_user = function(id){
        window.db.contactGet(id).then(function(user){
            var figure = window.load_chart(user.date, user.month, user.year);
            window.load_name(user.name, figure.r);

            $('#user__title').text(user.name + ' (' + figure.r + ')');
            $('#edit-form__desc').text(user.desc); 

            window.user = user;
        });
    }; 

    /* Deleting users */
    window.delete_user = function(){
        //todo: loading...
        window.db.contactDelete(window.user).then(function(){
            window.load_users();
            window.router.navigate('/page/main');
        });
    };

    /* Adding account form */
    window.form_add = function(){
        var form = '#add-form #add-form__';    
        name = $(form+'name')[0].value;
        date = $(form+'date')[0].value;
        mnth = $(form+'month')[0].value;
        year = $(form+'year')[0].value;
        
        if(!name || !date || !mnth || !year){
            alert('Please enter all the details');
            return false;
        }
        
        window.db.contactCreate({
            name: name,
            date: date,
            mnth: mnth,
            year: year
        }).then(function(){
            $(form+'name')[0].value = '';
            $(form+'date')[0].value = '';
            $(form+'month')[0].value = '';
            $(form+'year')[0].value = '';
            
            window.load_users();
            window.router.navigate('/page/main');
        }).catch(function(e){
            alert(e);  
        });

        return false;
    };

    /* Adding description */
    window.form_save = function(){
        window.user.desc = $('#edit-form__desc')[0].value;
        window.db.contactUpdate(window.user).then(function(){
            alert('saved');
            window.load_user(window.user._id);
        });
    }
    
    /* Loading chart */
    window.load_chart = function(date, month, year, ele){
        if(!ele){ ele = '#figure'; }

        var figure = calc.figure(date, month, year);
        var template = Handlebars.compile($('#figureTemplate').html()); 
        $(ele).html(template(figure));
        
        var elements = 'earth metal water fire wood'.split(' ');
        $(ele+' #highlight').addClass(elements[figure.r%5]);
        
        if(ele == '#figure'){
            window.figure = figure;
        }

        return figure;
    }

    /* loading names */
    window.load_name = function(name, lifepath){
        var num = calc.words(name);
        var rating = calc.rate(lifepath);
        var pos = rating.indexOf(num%5||5);
        $('#panel-name #name').text(name);
        $('#panel-name #number').text(num); 
        $('#panel-name #bar')[0].innerHTML = '<div class="bar bar'+(pos+1).toString()+'"></div>';

        var descs = [
            'Your name is same as your lifepath.',
            'You control your name.',
            'Your name supports you.',
            'You support your name.',
            'Your name controls you.'
        ];
        $('#panel-name #desc').text(descs[pos]);
    };
    
    // frontend function
    window.tryName = function(){
        var name = $('#name-form__name')[0].value;
        var lifepath = parseInt(window.figure.r);
        window.load_name(name, lifepath);
    };

    window.changeName = function(){ 
        window.user.name = $('#panel-name #name').text();

        window.db.contactUpdate(window.user).then(function(){
            alert('saved');
            window.load_users();
            window.load_user(window.user._id);
        });
    };

    /* calculator function */
    window.calcWord = function(){
        var title = $('#word-form__title')[0].value;
        $('#name-title').text(title.toLowerCase());
        $('#name-num').text(calc.words(title));
    };
        
    window.calcDate = function(){
        var date = $('#date-form__date')[0].value;
        var month = $('#date-form__month')[0].value;
        var year = $('#date-form__year')[0].value;

        if(!date || !month || !year){
            alert('Please enter all the details.');
            return;
        }

        window.load_chart(date, month, year, '#date-chart');
    };

    /* Calculating compatability */
    window.calcComp = function(){
        var date = $('#comp-form__date')[0].value;
        var month = $('#comp-form__month')[0].value;
        var year = $('#comp-form__year')[0].value;

        if(!date || !month || !year){
            alert('Please enter all the details.');
            return;
        }

        var figure = window.load_chart(date, month, year, '#comp-chart');
        var user = window.user;
        
        var temp = $('#comp-chart').html();
        $('#comp-chart').html("<h4>Other Guy's Chart</h4>"+temp);

        var i = function(s){ return parseInt(s); };
        var s = function(i){ return i.toString(); };

        var dd = s(i(date) + i(user.date));
        var mm = s(i(month) + i(user.month));
        var yyyy = s(i(figure.y1)+i(user.year.slice(0,2))) + s(i(figure.y2)+i(user.year.slice(2)));
        
        var comp_fig = window.load_chart(dd, mm, yyyy, '#comp-chart2');
        var temp = $('#comp-chart2').html();
        $('#comp-chart2').html('<h4>Compatability Chart</h4>'+temp);
        $('#comp-chart2 #figure-dd').html(' ');
        $('#comp-chart2 #figure-mm').html(' ');
        $('#comp-chart2 #figure-y1').html(' ');
        $('#comp-chart2 #figure-y2').html(' ');


        $('#panel-comp #comp-details')[0].style.display = 'block';
 
        var n2=0, n7=0;
        for(k in comp_fig){
            var v = comp_fig[k];
            if(v.length>1){ continue; }

            if(v=='2'){ n2+=1; }
            if(v=='7'){ n7+=1; }
        }
        var score = n2*n7;
        
        if(score>5){ score=5; }
        var texts = [
            'Not compatible',
            'Okay, not particularly exiting',
            '3rd party involved, but will still be okay',
            '3rd party involved, but will still be good',
            'Very compatible together',
            'Will face problems, but will be resolved'
        ];
        text = texts[score];

        $('#comp-details #text')[0].innerHTML = text;
        $('#panel-comp .rating-bar')[0].innerHTML = '<div class="bar bar'+ score +'"></div>';
    };
    
    /* Clear data, empties form, clear charts */
    window.clearData = function(){
        //user data
        window.user = {};
        window.figure = {};
        
        $('#user__title').html('');
        $('#panel-figure #figure').html('');

        $('#panel-name #name').html('');
        $('#panel-name #number').html('');
        $('#panel-name #bar').html('');
        $('#panel-name #desc').html('');
        $('#panel-name #name-form__name')[0].value = '';

        $('#panel-description #edit-form__desc')[0].value = '';
        
        $('#comp-form__date')[0].value = '';
        $('#comp-form__month')[0].value = '';
        $('#comp-form__year')[0].value = '';
        $('#comp-details')[0].style.display = 'none';
        $('#comp-chart').html('');
        $('#comp-chart2').html('');

        $('#pg-calcword #name-title').html('numerlogy');
        $('#pg-calcword #name-num').html('4');
        $('#pg-calcword #word-form__title')[0].value = '';

        $('#date-form__date')[0].value = '';
        $('#date-form__month')[0].value = '';
        $('#date-form__year')[0].value = '';
        $('#date-chart').html('');

        $('#search-form__search')[0].value = '';
        $('#pg-search .content').html('');
    };
    
    /* Search for specific users */
    window.searchUsers = function(){
        var search = $('#search-form__search')[0].value;
        search.toLowerCase();

        window.db.getAll().then(function(d){
            var users2 = [];
            var users = d.rows.map(function(r){
                var user = r.doc;
                user.name_l = user.name.toLowerCase();
                user.idx = user.name_l.indexOf(search);

                if(user.idx+1){ users2.push(user); };
            }); 

            users2.sort(function(a,b){
                if(a.idx==b.idx){
                    return a.name_l > b.name_l;
                }

                return a.idx > b.idx;
            });
            
            users = users2.map(function(user){
                user.lifepath = calc.lifepath(user.date, user.month, user.year);
                return user;
            });

            if(!users.length){
                $('#pg-search .content').html('<h3 class="contact__empty">Nothing found</h3>');
            }else{
                var template = Handlebars.compile($('#template__users').html());
                $('#pg-search .content').html(template({users:users}));
            }

        }).catch(function(e){
            alert(e);
        }); 
    };
    
    /* Star contact */
    window.starContact = function(id){
        //TODO 
    };

    window.load_users();
});


